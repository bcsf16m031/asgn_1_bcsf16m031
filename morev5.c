/* 
*  Video Lecture 11
*  morev4.c: handle io rediretion....reverse video featureread and print one page then pause for a few special commands ('q', ' ' , '\n')
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>

int LINELEN;
int PAGELEN;
int totalLines;
char fname[30];

void do_more(FILE *);
int  get_input(FILE*);

struct termios saved_attributes;

void
reset_input_mode (void)
{
  tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);
}

void
set_input_mode (void)
{
  struct termios tattr;
  char *name;

  if (!isatty (STDIN_FILENO))
    {
      fprintf (stderr, "Not a terminal.\n");
      exit (EXIT_FAILURE);
    }

  tcgetattr (STDIN_FILENO, &saved_attributes);
  atexit (reset_input_mode);

  tcgetattr (STDIN_FILENO, &tattr);
  tattr.c_lflag &= ~(ICANON|ECHO);
  tattr.c_cc[VMIN] = 1;
  tattr.c_cc[VTIME] = 0;
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &tattr);
}


int main(int argc , char *argv[])
{
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   int i=0;
   if (argc == 1){
      do_more(stdin);
   }
   FILE * fp;
   while(++i < argc){
      strcpy(fname, argv[i]);
      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
      totalLines = 0;
      char buf[LINELEN];
      int length = 0;
      while (fgets(buf, LINELEN, fp)!=NULL){
	 totalLines=totalLines+1;
	 length += strlen(buf);
      }
      fseek(fp,-length, SEEK_CUR);
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

void do_more(FILE *fp)
{
   int num_of_lines = 0;
   int rv;
   int totalLinesRead = 0;
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   char buffer[LINELEN];
   FILE* fp_tty = fopen("/dev//tty", "r");
   while (fgets(buffer, LINELEN, fp)){
      LINELEN = wbuf.ws_col;
      PAGELEN = wbuf.ws_row-1;
      fputs(buffer, stdout);
      totalLinesRead++;
      num_of_lines++;
      if (num_of_lines == PAGELEN){
	 float percRead = ((float)totalLinesRead/(float)totalLines) * 100;
	 printf("\033[7m --more--(%.2f%c)\033[m",percRead,'%');
         rv = get_input(fp_tty);	
         if (rv == 0){//user pressed q
            printf("\033[1A \033[2K \033[1G");
            break;//
         }
         else if (rv == 1){//user pressed space bar
            printf("\033[1A \033[2K \033[1G");
            num_of_lines -= PAGELEN;
         }
         else if (rv == 2){//user pressed return/enter
            printf("\033[1A \033[2K \033[1G");
	         num_of_lines -= 1; //show one more line
            }
	 else if (rv == 3){ //search
            printf("\033[2K \033[0G/");
            char str[30];
	    scanf("%s",str);
	    char buff[30];
	    int length = 0;
	    int found = 0;
	    while(fgets(buff,30, fp)!=NULL){
		int N=strlen(buff);
		int M=strlen(str);
		for (int i = 0; i <= N - M; i++) { 
		int j; 
		/* For current index i, check for pattern match */
			for (j = 0; j < M; j++) 
				if (buff[i + j] != str[j]) 
					break; 
			if (j == M) 
				found = 1; 
	        }   
		if(found == 1){
			printf("\n\033[1A \033[2K \033[0GSkipping...\n");
			fseek(fp,-3*strlen(buff),SEEK_CUR);
			num_of_lines -= PAGELEN;
			length = 0;
			break;
		}
		length+=strlen(buff);
	    }
	    if(found == 0){
		printf("\033[1A\033[7mPattern Not Found \033[m");
		fseek(fp,-length,SEEK_CUR);
		length = 0;
	    } 
         }
	 else if(rv == 4){
		printf("hello");	
               int pid = fork();
               if(pid == 0);
        		char * args[2];
			args[0] = "vim";
			args[1] = "../longfile.txt";
        		int ret=execl("/usr/bin/vim", "vim", fname);
         }
         else if (rv == 5){ //invalid character
            printf("\033[1A \033[2K \033[1G");
            break; 
         }
      }
  }
}

int get_input(FILE* cmdstream)
{
      int c;
      set_input_mode();		
      c = getc(cmdstream);
      printf("\033[1K");
      reset_input_mode();
      if(c == 'q')
	 return 0;
      if ( c == ' ' )			
	 return 1;
      if ( c == '\n' )	
	 return 2;
      if( c == '/' )
	 return 3;
      if( c == 'v' )
	 return 4;	
      return 5;
   return 0;
}
