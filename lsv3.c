#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

extern int errno;

char* getFileMode(char *file){
    struct stat st;
    char *modeval = malloc(sizeof(char) * 10 + 1);
    if(stat(file, &st) == 0){
        mode_t perm = st.st_mode;
	if ((st.st_mode &  0170000) == 0010000) 
		modeval[0] = 'p';
	else if ((st.st_mode &  0170000) == 0020000) 
		modeval[0] = 'c';
	else if ((st.st_mode &  0170000) == 0040000) 
		modeval[0] = 'd';
   	else if ((st.st_mode &  0170000) == 0060000) 
		modeval[0] = 'b';
	else if ((st.st_mode &  0170000) == 0100000) 
		modeval[0] = 'f';
	else if ((st.st_mode &  0170000) == 0120000) 
		modeval[0] = 'l';
   	else if ((st.st_mode &  0170000) == 0140000) 
		modeval[0] = 's';
   	else 
		printf("Unknown mode\n");
        modeval[1] = (perm & S_IRUSR) ? 'r' : '-';
        modeval[2] = (perm & S_IWUSR) ? 'w' : '-';
        modeval[3] = (perm & S_IXUSR) ? 'x' : '-';
        modeval[4] = (perm & S_IRGRP) ? 'r' : '-';
        modeval[5] = (perm & S_IWGRP) ? 'w' : '-';
        modeval[6] = (perm & S_IXGRP) ? 'x' : '-';
        modeval[7] = (perm & S_IROTH) ? 'r' : '-';
        modeval[8] = (perm & S_IWOTH) ? 'w' : '-';
        modeval[9] = (perm & S_IXOTH) ? 'x' : '-';
        modeval[10] = '\0';
        return modeval;     
    }
    else{
        return strerror(errno);
    }   
}

int filterHiddenDir(const struct dirent * dir){
	if(dir->d_name[0]=='.')
		return 0;
	return 1;
}

int alphaCmp(void * a, void * b){
	return strcasecmp((*(struct dirent **)a)->d_name, (*(struct dirent **)b)->d_name);
}

void do_ls(char * dir)
{
   struct dirent ** entryList;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   int n = scandir(dir, &entryList, &filterHiddenDir, alphaCmp);
   if (n == -1){
	printf("Directory scanning error");
	return;
   }
   int i = 0;
   while(i < n){
	char newpath[4096]="";
	strcat(newpath, dir);
	strcat(newpath, "/");
	strcat(newpath, entryList[i]->d_name);
	char * filemode = getFileMode(dir);
	struct stat sb;
	stat(newpath, &sb);
	struct passwd * pwd = getpwuid(sb.st_uid);
	struct group * grp = getgrgid(sb.st_gid);
	//printf("%s", ctime(&sb.st_mtime));
	printf("%6ld %10s %2d %10s %10s %8ld %.24s %-20s\n", entryList[i]->d_ino, filemode, sb.st_nlink, pwd->pw_name, grp->gr_name, sb.st_size, ctime(&sb.st_mtime), entryList[i]->d_name);
	free(entryList[i]);
	free(filemode);
	i++;
   }
   free(entryList);
   closedir(dp);
}

int main(int argc, char* argv[]){
   if (argc == 1){
	char cwd[4096];
	getcwd(cwd, sizeof(cwd));
        printf("Directory listing of pwd:\n");
      	do_ls(cwd);
   }
   else{
      int i = 0;
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	 do_ls(argv[i]);
      }
   }
   
   return 0;
}
