/* 
*  Video Lecture 11
*  morev4.c: handle io rediretion....reverse video featureread and print one page then pause for a few special commands ('q', ' ' , '\n')
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

int LINELEN;
int PAGELEN;
int totalLines;

void do_more(FILE *);
int  get_input(FILE*);
int main(int argc , char *argv[])
{
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   int i=0;
   if (argc == 1){
      do_more(stdin);
   }
   FILE * fp;
   while(++i < argc){
      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
      totalLines = 0;
      char buf[LINELEN];
      int length = 0;
      while (fgets(buf, LINELEN, fp)!=NULL){
	 totalLines=totalLines+1;
	 length += strlen(buf);
      }
	printf("total lines: %d", totalLines);
      fseek(fp,-length, SEEK_CUR);
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

void do_more(FILE *fp)
{
   int num_of_lines = 0;
   int rv;
   int totalLinesRead = 0;
   struct winsize wbuf;
   if(ioctl(0, TIOCGWINSZ, &wbuf) == -1){
      perror("Error in ioctl");
      exit(1);
   }
   LINELEN = wbuf.ws_col;
   PAGELEN = wbuf.ws_row-1;
   char buffer[LINELEN];
   FILE* fp_tty = fopen("/dev//tty", "r");
   while (fgets(buffer, LINELEN, fp)){
      fputs(buffer, stdout);
      totalLinesRead++;
      num_of_lines++;
      if (num_of_lines == PAGELEN){
	 float percRead = ((float)totalLinesRead/(float)totalLines) * 100;
	 printf("\033[7m --more--(%.2f%c)\033[m",percRead,'%');
         rv = get_input(fp_tty);		
         if (rv == 0){//user pressed q
            printf("\033[1A \033[2K \033[1G");
            break;//
         }
         else if (rv == 1){//user pressed space bar
            printf("\033[1A \033[2K \033[1G");
            num_of_lines -= PAGELEN;
         }
         else if (rv == 2){//user pressed return/enter
            printf("\033[1A \033[2K \033[1G");
	         num_of_lines -= 1; //show one more line
            }
         else if (rv == 3){ //invalid character
            printf("\033[1A \033[2K \033[1G");
            break; 
         }
      }
  }
}

int get_input(FILE* cmdstream)
{
   int c;		
     c = getc(cmdstream);
      if(c == 'q')
	 return 0;
      if ( c == ' ' )			
	 return 1;
      if ( c == '\n' )	
	 return 2;	
      return 3;
   return 0;
}
