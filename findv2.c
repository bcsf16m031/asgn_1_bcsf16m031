#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#define PATH_MAX 4096
extern int errno;

int isDirectory(const char *path) {
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
       return 0;
   return S_ISDIR(statbuf.st_mode);
}

int find(char drc[], char fname[], char * type){
	DIR * dp = opendir(drc);
	if(dp == NULL) { 
		printf("No such directory\n"); 
		return 0;
	}; 
	chdir(drc);
	errno = 0;
	struct dirent* entry;
	while(1) {
		entry = readdir(dp);
		//printf("%s\n", entry->d_name);
		if(entry == NULL && errno != 0){
		    //perror("readdir");
		    return errno;
	        }
	        if(entry == NULL && errno == 0){
		    //printf("\nEnd of directory %s\n", drc);
		    closedir(dp);
		    return 0;
	        }
		if(entry->d_name[0] == '.'){
		    continue;
		}

		char newpath[4096] = "";
		strcat(newpath, drc);
		strcat(newpath, "/");
		strcat(newpath, entry->d_name);

		struct stat sb;
		stat(newpath, &sb);
		switch(type[0]){
		case 'f':
           		if (S_ISREG(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case 'd':
			if (S_ISDIR(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case 'b':
			if (S_ISBLK(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case 'c':
			if (S_ISCHR(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case 'p':
			if (S_ISFIFO(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case 'l':
			if (S_ISLNK(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case 's':
			if (S_ISSOCK(sb.st_mode)) {
				if(strcmp(fname, "*") == 0){ printf("%s \n", newpath); }
               			if(strcmp(entry->d_name, fname) == 0){
					printf("%s \n", newpath);
				}
           		}
			break;
		case '*':
			if(strcmp(entry->d_name, fname) == 0){
				printf("%s \n", newpath);
			}
			break;
		default:
			printf("Invalid filetype\n");
			return 0;
		}
		if(isDirectory(newpath) == 1) {
			find(newpath, fname, type);
		}
	}
}

int main(int argc, char * argv[]){
	char cwd[PATH_MAX];
	getcwd(cwd, sizeof(cwd));
	char * type = "*";
	char * fname = "*";
	for(int i = 0; i < argc; i++){
		if(strcmp(argv[i], "-type") == 0) {
			type = argv[i+1];
		}
		if(strcmp(argv[i], "-name") == 0) {
			fname = argv[i+1];
		}
	}
	if(strcmp(argv[1], ".") == 0){
		find(cwd, fname, type);
	}
	else{
		find(argv[1], fname, type);
	}
   return 0;
}
